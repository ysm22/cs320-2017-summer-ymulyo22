(* ****** ****** *)

#include
"./../midterm-2.dats"

(* ****** ****** *)

fun switch_values(xs: list0(int), x: int, y: int): list0(int)=
    list0_map(xs,lam n => if n = x then y else if n = y then x else n)

fun split_and_reverse(xs: list0(int), i: int): list0(int)=
case xs of
|list0_nil() => list0_nil()
|list0_cons(x1,xs2) => if x1 = i
                       then list0_cons(x1,list0_reverse(xs2))
                       else list0_cons(x1,split_and_reverse(xs2,i))

implement
mylist_perm_succ(xs: list0(int)): list0(int) =
let fun aux(xs1: list0(int), prev_xs: list0(int), first_n: int, pivot: bool): list0(int) =
    if not(pivot)
    then case xs1 of
         |list0_nil() => $raise(LastPermExn)
         |list0_cons(x1,xs2) => if prev_xs.head() > x1
                                then aux(list0_reverse(xs), xs1, x1, true)
                                else aux(xs2, xs1, 0, pivot)
    else case xs1 of
         |list0_nil() => $raise(LastPermExn)
         |list0_cons(x1,xs2) => if x1 > first_n
                                then split_and_reverse(switch_values(xs,first_n,x1),x1)
                                else aux(xs2, xs1, first_n, true)
in
    if list0_length(xs) < 2
    then list0_nil()
    else try aux((list0_reverse(xs)).tail(),list0_reverse(xs), 0, false) with ~LastPermExn() => list0_nil()
end
         
    
implement main0() =
{
val
xs0 =
g0ofg1($list{int}(0,1,2))
val xys = mylist_perm_succ(xs0)
val () = println!(xys)
val xs0 =
g0ofg1($list{int}(0,2,1))
val xys = mylist_perm_succ(xs0)
val () = println!(xys)
val xs0 =
g0ofg1($list{int}(1,0,2))
val xys = mylist_perm_succ(xs0)
val () = println!(xys)
val xs0 =
g0ofg1($list{int}(1,2,0))
val xys = mylist_perm_succ(xs0)
val () = println!(xys)
val xs0 =
g0ofg1($list{int}(2,0,1))
val xys = mylist_perm_succ(xs0)
val () = println!(xys)
val xs0 =
g0ofg1($list{int}(2,1,0))
val xys = mylist_perm_succ(xs0)
val () = println!(xys)
}
 (* end of [main0] *)
//
(* ****** ****** *)

(* end of [mylist_perm_succ.dats] *)
