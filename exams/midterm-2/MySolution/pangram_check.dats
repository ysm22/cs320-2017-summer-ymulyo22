(* ****** ****** *)

#include
"./../midterm-2.dats"

(* ****** ****** *)



implement pangram_check(text) = 
let fun helper(xs: list0(char), uniqueChar: list0(char)): bool =
case xs of
|list0_nil() => list0_length(uniqueChar) >= 26
|list0_cons(x,xs1) => if list0_exists(uniqueChar, lam y => (y = toupper_char(x) )) && isalpha_char(x) then helper(xs1, uniqueChar) else helper(xs1, list0_cons(toupper_char(x), uniqueChar)) 
in
    helper(string_explode(text), list0_nil())
end


(* ****** ****** *)
//
implement main0() =
{
val () = println!(pangram_check("Pack my box with five dozen liquor jugs."),pangram_check("whaidwj dwa adwnoaini awdidai iwaj dioa"))
} (* end of [main0] *)
//
(* ****** ****** *)

(* end of [pangram_check.dats] *)
