(* ****** ****** *)

#include
"./../midterm-2.dats"

(* ****** ****** *)

fun take_highest(x: int, y: int):int = if (x > y) then x else y
fun diff_is_less_than_2(x: int, y: int): list0(int) = if (x-y < 2) && (x-y > ~2) then list0_cons(take_highest(x,y),list0_nil()) else list0_nil()

implement {a} mytree_avltest(t0: mytree(a)): bool =
let exception NOTAVL fun aux(t0: mytree(a)): int =
case t0 of
|mytree_nil() => 0
|mytree_cons(lt, _, rt) => case diff_is_less_than_2(aux(lt),aux(rt)) of
                           |list0_nil() => $raise(NOTAVL)
                           |list0_cons(x,_) => x + 1
in
    (try aux(t0) with ~NOTAVL() => ~1) > ~1
end
    


(* ****** ****** *)
//
implement main0() =
{
val xs0 = mytree_nil()
val xs1 = mytree_cons(xs0, 1, xs0)
val xs2 = mytree_cons(xs1, 1, xs0)
val xs3 = mytree_cons(xs2, 1, xs0)
val xs4 = mytree_cons(xs3, 1, mytree_cons(xs1, 1, xs1))
val () = println!(mytree_avltest<int>(xs0),mytree_avltest<int>(xs1),mytree_avltest<int>(xs2),mytree_avltest<int>(xs3),mytree_avltest<int>(xs4))
} (* end of [main0] *)
//
(* ****** ****** *)

(* end of [mytree_avltest.dats] *)
