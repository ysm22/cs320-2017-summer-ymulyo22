(* ****** ****** *)

#include
"./../midterm-2.dats"

(* ****** ****** *)


implement {a} mystream_group(xs) =
let fun helper(tp: $tup(a,int), xss: stream(a)): stream($tup(a,int))=
case !xss of
|stream_nil() => $delay(stream_cons(tp, $delay(stream_nil())))
|stream_cons(x:a, xs1) => if geq_val_val<a>(tp.0, x)
                        then helper($tup(x,tp.1+1), xs1)
                        else $delay(stream_cons(tp,helper($tup(x,1),xs1)))
in
case !xs of 
|stream_nil() => $delay(stream_nil())
|stream_cons(x,xs1) => helper($tup(x,1),xs1)
end



(* ****** ****** *)
//
implement main0() =
{
val xs0:stream(string) =
$delay(stream_cons("b",
$delay(stream_cons("i",
$delay(stream_cons("l",
$delay(stream_cons("l",
$delay(stream_nil())
))))))))

val x = mystream_group<string>(xs0)
val x1 = stream_nth_exn(x,0)
val () = println!(x1.0,x1.1)
} (* end of [main0] *)
//
(* ****** ****** *)

(* end of [mystream_group.dats] *)
