(* ****** ****** *)

#include
"./../midterm-2.dats"

(* ****** ****** *)
fun check235(i: int): bool =
 if i = 1
 then true
 else if i % 2 = 0
 then check235(i/2)
 else if i % 3 = 0
 then check235(i/3)
 else if i % 5 = 0
 then check235(i/5)
 else false
 

implement mystream_235() = 
let fun helper(i: int): stream(int)=
    if check235(i)
    then $delay(stream_cons(i,helper(i+1)))
    else helper(i+1)
in
    helper(1)
end

(* ****** ****** *)
//
implement main0() =
{
val xs = stream_nth_exn(mystream_235(), 0)
val () = println!(xs)
val xs = stream_nth_exn(mystream_235(), 1)
val () = println!(xs)
val xs = stream_nth_exn(mystream_235(), 2)
val () = println!(xs)
val xs = stream_nth_exn(mystream_235(), 3)
val () = println!(xs)
val xs = stream_nth_exn(mystream_235(), 4)
val () = println!(xs)
val xs = stream_nth_exn(mystream_235(), 5)
val () = println!(xs)
val xs = stream_nth_exn(mystream_235(), 6)
val () = println!(xs)
val xs = stream_nth_exn(mystream_235(), 7)
val () = println!(xs)
val xs = stream_nth_exn(mystream_235(), 8)
val () = println!(xs)
val xs = stream_nth_exn(mystream_235(), 9)
val () = println!(xs)
val xs = stream_nth_exn(mystream_235(), 10)
val () = println!(xs)
val xs = stream_nth_exn(mystream_235(), 11)
val () = println!(xs)
} (* end of [main0] *)
//
(* ****** ****** *)

(* end of [mystream_235.dats] *)
