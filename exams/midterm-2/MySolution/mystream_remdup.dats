(* ****** ****** *)

#include
"./../midterm-2.dats"

(* ****** ****** *)


implement {a} mystream_remdup(xs: stream(a), eqfn: (a, a) -<cloref1> bool): stream(a) =
let fun helper(xs:stream(a), existing_A:list0(a)):stream(a)=
case !xs of
|stream_nil() => $delay(stream_nil())
|stream_cons(x,xs1) => if list0_exists<a>(existing_A, lam y => eqfn(x,y))
                       then helper(xs1, existing_A)
                       else $delay(stream_cons(x, helper(xs1, list0_cons(x,existing_A))))
in
    helper(xs,list0_nil())
end


(* ****** ****** *)
//
implement main0() =
{
val xs0:stream(int) =
$delay(stream_cons(1,
$delay(stream_cons(1,
$delay(stream_cons(2,
$delay(stream_cons(3,
$delay(stream_cons(1,
$delay(stream_cons(2,
$delay(stream_cons(4,
$delay(stream_cons(3,
$delay(stream_cons(5,
$delay(stream_cons(6,
$delay(stream_cons(1,
$delay(stream_cons(1,
$delay(stream_cons(5,
$delay(stream_cons(6,
$delay(stream_cons(7,
$delay(stream_cons(8,
$delay(stream_nil())
))))))))))))))))))))))))))))))))
val x = mystream_remdup<int>(xs0, lam (x,y) => x=y)
val x1 = stream_nth_exn(x,0)
val () = println!(x1)
val x1 = stream_nth_exn(x,1)
val () = println!(x1)
val x1 = stream_nth_exn(x,2)
val () = println!(x1)
val x1 = stream_nth_exn(x,3)
val () = println!(x1)
val x1 = stream_nth_exn(x,4)
val () = println!(x1)
val x1 = stream_nth_exn(x,5)
val () = println!(x1)
val x1 = stream_nth_exn(x,6)
val () = println!(x1)
val x1 = stream_nth_exn(x,7)
val () = println!(x1)
} (* end of [main0] *)
//
(* ****** ****** *)

(* end of [mystream_remdup.dats] *)
