(* ****** ****** *)

#include
"./../midterm-2.dats"

(* ****** ****** *)


implement Wallis() = 
let fun helper(x: double, y: double, prev: double): stream(double)=
$delay(
    if x > y
    then stream_cons(x*prev/y, helper(x, y+2,x*prev/y))
    else stream_cons(x*prev/y, helper(x+2,y,x*prev/y)))
in
    helper(2.0,1.0, 1.0)
end


(* ****** ****** *)
//
implement main0() =
{
//
// Please write your own test
//
val Wallis_1M = stream_nth_exn(Wallis(), 1000000)
val () = println! ("Wallis_1M = ", Wallis_1M)
} (* end of [main0] *)
//
(* ****** ****** *)

(* end of [Wallis.dats] *)
