(* ****** ****** *)

#include
"./../midterm-2.dats"

(* ****** ****** *)
fun {a:t@ype}
mylist0_mcons
(
  x0: a, xss: list0(list0(a))
) : list0(list0(a)) =
(
  case+ xss of
  | list0_nil() =>
    list0_nil()
  | list0_cons(xs, xss) =>
    list0_cons
      (list0_cons(x0, xs), mylist0_mcons(x0, xss))
    // end of [list0_cons]
)

fun {a:t@ype}
double_list_perE(xs: list0(a)): list0(list0(a)) =
list0_map(xs, lam x => list0_cons(x, list0_nil()))

fun {a:t@ype}
mylist0_choose(xs: list0(a), n: int): list0(list0(a)) =
case xs of
| list0_nil() => list0_nil()
| list0_cons(x1,xs2) => if n <= 0
                        then list0_nil()
                        else if n = 1
                        then double_list_perE(xs)
                        else if n = 2
                        then mylist0_mcons(x1,double_list_perE(xs2)) + mylist0_choose(xs2,n)
                        else mylist0_mcons(x1,mylist0_choose(xs2,n-1)) + mylist0_choose(xs2,n)

fun {a:t@ype} remove_all(xs: list0(a), ys: list0(list0(a))): list0(a) =
let fun helper(xs: list0(a), ys: list0(a)): list0(a) =
case ys of
|list0_nil() => xs
|list0_cons(y, ys1) => helper(list0_filter(xs,lam x => not(geq_val_val(x,y))), ys1)
in
case ys of
|list0_nil() => xs
|list0_cons(y, ys1) => helper(xs,y)
end

fun {a:t@ype} take_after_n(xs: list0(a), n: int): list0(a)=
if n > 0

then    case xs of
        |list0_nil() => list0_nil()
        |list0_cons(x,xs1) => take_after_n(xs1,n-1)
else    xs

implement {a} mylist_select
(
xs: list0(a), n: int
) : stream($tup(list0(a), list0(a))) =
let fun helper(xs: list0(a), xs1: list0(list0(a)), xs2: list0(list0(a))): stream($tup(list0(a),list0(a))) =
case xs1 of
|list0_nil() => $delay(stream_nil())
|list0_cons(x1,xs11) => case xs2 of
                        |list0_nil() => helper(xs,xs11,mylist0_choose(remove_all(xs,xs11),list0_length(xs)-n))
                        |list0_cons(x2, xs21) => $delay(stream_cons($tup(x1,x2),helper(xs,xs1,xs21)))
in
    if n < 1
    then $delay(stream_nil())
    else helper(xs,mylist0_choose(xs,n),mylist0_choose(take_after_n(xs,n),list0_length(xs)-n))
end
(* ****** ****** *)
//
implement main0() =
{
val xs0 = g0ofg1($list{int}(1,2,3,4))
val x = mylist_select<int>(xs0,2)
val xs = stream_nth_exn(x, 0)
val () = println!(xs.0,xs.1)
val xs = stream_nth_exn(x, 1)
val () = println!(xs.0,xs.1)
val xs = stream_nth_exn(x, 2)
val () = println!(xs.0,xs.1)
val xs = stream_nth_exn(x, 3)
val () = println!(xs.0,xs.1)
val xs = stream_nth_exn(x, 4)
val () = println!(xs.0,xs.1)
val xs = stream_nth_exn(x, 5)
val () = println!(xs.0,xs.1)
} (* end of [main0] *)
//
(* ****** ****** *)

(* end of [mylist_select.dats] *)
