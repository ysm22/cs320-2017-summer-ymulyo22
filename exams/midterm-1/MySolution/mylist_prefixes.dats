(* ****** ****** *)


#include "./../midterm-1.dats"

(* ****** ****** *)
(*
HX: 10 points
[mylist_prefixes] returns a list
consisting of all the prefixes of a given list.
For instance, given (0, 1, 2), mylist_prefixes
returns the following list of lists:
//
        ((), (0), (0, 1), (0, 1, 2))
//
*)
implement
{a}
mylist_prefixes(xs: list0 (a)): list0(list0(a))=
let fun helper(xs:list0(a), i : int, result: list0(list0(a))): list0(list0(a))=
    let fun itterator(xs:list0(a), i: int, result: list0(a)): list0(list0(a)) =
        if i <= 0
        then list0_cons(result,list0_nil())
        else case xs of
             | list0_nil() => list0_nil()
             | list0_cons(x1,xs2) => itterator(xs2, i-1, result + list0_cons(x1,list0_nil()))
    in
        if i >= list0_length(xs)
        then result
        else if i = 0
        then helper(xs,i+1,result)
        else
        helper(xs,i+1,result + itterator(xs, i, list0_nil()))
    end
in
    helper(xs,0,list0_cons(list0_nil(),list0_nil()))
end

implement main0() =
{
val
xs0 =
g0ofg1($list{int}(1,2,3,4,5,6))
val xys = mylist_prefixes(xs0)
val () = list0_foreach<list0(int)> (xys, lam (x) => println! (x))
}


    
//