(* ****** ****** *)


#include "./../midterm-1.dats"

(* ****** ****** *)
(* ****** ****** *)
//
// HX: 10 points
//
// Given a binary tree and a predicate,
// [mytree_filter] returns a list consisting of
// all the elements in the tree that satisfiyes
// the predicate.
//
implement
{a}
mytree_filter(ts: tree0(a), p: (a) -> bool): list0(a) =
case ts of
|tree0_nil() => list0_nil()
|tree0_cons(t1, x, t2) => if p(x)
                          then list0_cons(x,list0_nil()) + mytree_filter(t1,p) + mytree_filter(t2,p)
                          else mytree_filter(t1,p) + mytree_filter(t2,p)

implement main0() =
{
val
xs0 = tree0_cons(tree0_cons(tree0_cons(tree0_nil(),4,tree0_nil()),2,tree0_nil()),1,tree0_cons(tree0_nil(),3,tree0_nil()))
val xys = mytree_filter(xs0, lam x => x mod 2 = 0)
val () = list0_foreach<int> (xys, lam (x) => println! (x))
}
//