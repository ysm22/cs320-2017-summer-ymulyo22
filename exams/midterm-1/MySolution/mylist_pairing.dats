(* ****** ****** *)


#include "./../midterm-1.dats"

(* ****** ****** *)

//
// Given a list xs = (x_1, x_2, ..., x_{n-1}, x_n),
// [mylist_pairing] returns ((x_1, x_n), (x_2, x_{n-1}), ...)
//
// Say xs = (1, 2, 3, 4, 5, 6). Then the result equals
// ((1, 6), (2, 5), (3, 4).
//
// You may assume that [xs] is always a list containing an even
// number of elements.
//
implement
{a}
mylist_pairing(xs: list0(a)): list0($tup(a,a)) =
let fun helper(xs: list0(a), rxs: list0(a), half_point: int, list_result: list0($tup(a,a))): list0($tup(a,a)) =
    if half_point <= 0
    then list_result
    else helper(xs.tail(), rxs.tail(), half_point - 1, list_result + list0_cons{$tup(a,a)}('(xs.head(),rxs.head()),list0_nil()))
in
    helper(xs,list0_reverse(xs),list0_length(xs)/2,list0_nil())
end



implement main0() =
{
val
xs0 =
g0ofg1($list{int}(1,2,3,4,5,6))
val xys = mylist_pairing(xs0)
//val ((*void*)) = print!("(", xys[2].0, ", ", xys[2].1, ")")
val tt = list0_map(xs0, lam x => $tup(x,2))
val () = list0_foreach<$tup(int,int)> (xys, lam (x) => println! (x.0,",",x.1))
}