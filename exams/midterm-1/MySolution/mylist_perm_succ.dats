(* ****** ****** *)


#include "./../midterm-1.dats"

(* ****** ****** *)
(*
//
// Q10: 20 points
//
// The permutations of 0,1,2 can be ordered
// according to the lexicographic ordering as follows:
//
// (0,1,2) < (0,2,1) < (1,0,2) < (1,2,0) < (2,0,1) < (2,1,0)
//
// This ordering can be readily generalized to the permuations
// of n numbers, which n is positive. Given a permutation xs of
// the first n natural numbers, perm_succ(xs) returns the next
// permutation following xs if it exists.
//
*)
//
fun switch_values(xs: list0(int), x: int, y: int): list0(int)=
    list0_map(xs,lam n => if n = x then y else if n = y then x else n)

fun split_and_reverse(xs: list0(int), i: int): list0(int)=
case xs of
|list0_nil() => list0_nil()
|list0_cons(x1,xs2) => if x1 = i
                       then list0_cons(x1,list0_reverse(xs2))
                       else list0_cons(x1,split_and_reverse(xs2,i))

implement
mylist_perm_succ(xs: list0(int)): option0(list0(int)) =
let fun helper(xs1: list0(int), prev_xs: list0(int), first_n: int, pivot: bool): option0(list0(int)) =
    if not(pivot)
    then case xs1 of
         |list0_nil() => None0()
         |list0_cons(x1,xs2) => if prev_xs.head() > x1
                                then helper(list0_reverse(xs), xs1, x1, true)
                                else helper(xs2, xs1, 0, pivot)
    else case xs1 of
         |list0_nil() => None0()
         |list0_cons(x1,xs2) => if x1 > first_n
                                then Some0(split_and_reverse(switch_values(xs,first_n,x1),x1))
                                else helper(xs2, xs1, first_n, true)
in
    if list0_length(xs) < 2
    then None0()
    else helper(list0_reverse(xs.tail()),list0_reverse(xs), 0, false)
end
         
    
implement main0() =
{
val
xs0 =
g0ofg1($list{int}(1,2,3,6,5,4))
val xys = mylist_perm_succ(xs0)
val () = case xys of
         |None0() => ()
         |Some0(x) => println!(x)
}

//