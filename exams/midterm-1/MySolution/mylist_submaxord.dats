(* ****** ****** *)


#include "./../midterm-1.dats"

(* ****** ****** *)
(* ****** ****** *)
//
// HX: 20 points
//
// Given a list xs of integers, the function
// [mylist_submaxord] returns the longest leftmost
// subsequence of xs that is ordered.
//
// If xs = (1, 3, 2, 4), then the result is
// (1, 3, 4) (not (1, 2, 4)).
//
// if xs = (0, 1, 3, 2, 2, 4), then the result is
// (0, 1, 2, 2, 4).
//
// If xs = (~1, ~1, 4, 1, 2, 3, 8, 9, 5, 6, 7), then the
// result is (~1, ~1, 1, 2, 3, 5, 6, 7)
//
fun longest_list(xs: list0(list0(int))): list0(int)=
let fun helper(xs: list0(list0(int)), longest: list0(int)): list0(int)=
case xs of
|list0_nil() => longest
|list0_cons(x1,xs2) => if list0_length(x1) > list0_length(longest)
                       then helper(xs2, x1)
                       else helper(xs2,longest)
in
    helper(xs,list0_nil())
end


implement
mylist_submaxord(xs: list0(int)): list0(int)=
let fun helper(xs: list0(int), all_sub: list0(list0(int))): list0(int) =
    let fun subsequence_finder(xs: list0(int), result: list0(int), last_n: int):list0(list0(int)) =
    case xs of
    |list0_nil() => list0_cons(result,list0_nil())
    |list0_cons(x1,xs2) => if x1 >= last_n
                           then subsequence_finder(xs2, result + list0_cons(x1,list0_nil()), x1)
                           else subsequence_finder(xs2, result, last_n)
    in
    case xs of
    |list0_nil() => longest_list(all_sub)
    |list0_cons(x1, xs2) => helper(xs2, all_sub + subsequence_finder(xs2, list0_cons(x1,list0_nil()), x1))
    end
in
    helper(xs,list0_nil())
end




implement main0() = {
val xs0 = g0ofg1($list{int}(1,3,2,3,4,5,6,4))
val xys = mylist_submaxord(xs0)
val () = println! (xys)
}