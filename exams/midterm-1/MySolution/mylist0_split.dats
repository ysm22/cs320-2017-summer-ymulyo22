(* ****** ****** *)


#include "./../midterm-1.dats"

(* ****** ****** *)
//
(*
** [mylist0_split] splits a list alternately.
** For instance, if the given list is (1, 2, 3, 4, 5)
** then it is splitted into (1,3,5) and (2,4)
*)
//
implement
{a}
mylist0_split(xs: list0(a)): $tup(list0(a), list0(a)) =
let fun alt_tracker(xs: list0(a), left_turn: bool, split_list: $tup(list0(a),list0(a))): $tup(list0(a),list0(a)) =
case xs of
| list0_nil() => $tup(list0_reverse(split_list.0),list0_reverse(split_list.1))
| list0_cons(x1, xs2) =>    if left_turn
                            then alt_tracker(xs2, false, $tup(list0_cons{a}(x1,split_list.0),split_list.1))
                            else alt_tracker(xs2, true, $tup(split_list.0,list0_cons{a}(x1,split_list.1)))
in
    alt_tracker(xs,true,$tup(list0_nil(),list0_nil()))
end

(* ****** ****** *)

implement main0() = 
{
val
xs0 =
g0ofg1($list{int}(1,2,3,4,5))
val xs1 = mylist0_split(xs0)
val xss = xs1.0
val () = println!(xs0)
val () = list0_foreach<int> (xss, lam (x) => print_int (x))
val () = list0_foreach<int> (xs1.1, lam (x) => print_int (x))
}
(* ****** ****** *)

//
(* ****** ****** *)