(* ****** ****** *)


#include "./../midterm-1.dats"

(* ****** ****** *)

(*
mylist2brauntree turns a given list xs into a Braun tree
such that flatten(mylist2brauntree(xs)) = xs
*)
implement
{a}
mylist2brauntree(xs: list0(a)): tree0(a) =
let fun indexer(i: int): tree0(a) =
    if i >= list0_length(xs)
    then tree0_nil()
    else tree0_cons(indexer(2*i + 1), xs[i], indexer(2*i + 2))
in
    indexer(0)
end



implement main0() = 
{
val
xs0 =
g0ofg1($list{int}(1,2,3,4,5))
val xs1 = mylist2brauntree(xs0)
val xs2 = mytree_fold(xs1, lam(l,h,r)=>  list0_cons(h,list0_nil()) + l + r, list0_nil())
val () = list0_foreach<int> (xs2, lam (x) => print_int (x))
}