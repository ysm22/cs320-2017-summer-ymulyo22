(* ****** ****** *)


#include "./../midterm-1.dats"

(* ****** ****** *)
(* ****** ****** *)
//
(*
**
** HX: 10 points
**
*)
//
// A list is a sublist of another list if the former can be obtained from
// removing some elements in the latter. For instance, (1,3,5) is a sublist
// of (1,2,3,4,5) but (1,1) is not a sublist of (1,2,3).
//
// Given two lists [xs] and [ys], the function sublist_test(xs, ys) returns
// true if and only if [ys] is a sublist of [xs]:
//
implement
sublist_test (xs: list0(int), ys: list0(int)): bool =
case ys of
|list0_nil() => true
|list0_cons(y1,ys2) => if list0_length(list0_filter(xs,lam x => x = y1)) = 0
                       then false
                       else sublist_test(list0_filter(xs,lam x => not(x = y1)), ys2)

implement main0() = {
val xs0 = g0ofg1($list{int}(1,2,3,4,5))
val xs1 = g0ofg1($list{int}(1,3,5))
val xs2 = g0ofg1($list{int}(1,1,5))
val xys = sublist_test(xs0,xs1)
val xyz = sublist_test(xs0,xs2)
val () = println! (xys,xyz)
}