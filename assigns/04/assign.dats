(* ****** ****** *)
//
// Title:
// Concepts of
// Programming Languages
// Course: CAS CS 320
//
// Semester: Summer I, 2017
//
// Classroom: KCB 102
// Class Time: MTWR 2:00-4:00
//
// Instructor:
// Hongwei Xi (hwxiATcsDOTbuDOTedu)
//
(* ****** ****** *)
//
// Assign04: 30 points
//
// Out date: Mon, June 12th, 2017
// Due date: Fri, June 16th, 2017 // by midnight
//
(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
#include
"share/HATS\
/atspre_staload_libats_ML.hats"
//
(* ****** ****** *)

(*
//
// Points: 10
//
The following is a well-known series:

ln 2 = 1 - 1/2 + 1/3 - 1/4 + ...

Please implement a stream consisting of all the partial sums of this
series. Then compute an accurate approximation to ln2 by using Euler's
transform (only if you do the following bonus question).
//
*)
extern
fun
stream_ln2(): stream(double)

(* ****** ****** *)
//
// Points: 10
//
// Please implement the following function that enumerates
// all the pairs of natural numbers (i, j) satisfying i <= j:
//
typedef int2 = $tup(int, int)
//
extern
fun
intpair_enumerate((*void*)): stream(int2)
//
(* ****** ****** *)
//
// HX: 10 *bonus* points
// Please implement the Euler transform:
// Given a sequence:
//   x(0), x(1), x(2), ..., x(n), ...
// We can form another sequence:
//   y(0), y(1), y(2), ..., y(n), ....
// such that each y(n) equals the the follow value
//
// x(n+2) - (x(n+2)-x(n+1))^2 / (x(n)+x(n+2)-2*x(n+1))
//
extern
fun
EulerTrans(xs: stream(double)): stream(double)
//
(* ****** ****** *)

(* end of [assign.dats] *)
