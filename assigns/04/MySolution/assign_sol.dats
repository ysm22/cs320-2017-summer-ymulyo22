(* ****** ****** *)
//
// HX:
//
// How to test:
// ./assign_sol
// How to compile:
// myatscc assign_sol.dats
//
(* ****** ****** *)
(*
\#\#myatsccdef=\
curl --data-urlencode mycode@$1 \
http://bucs320-tutoriats.rhcloud.com/\
SERVICE/assets/patsopt_tcats_eval_code_0_.php | \
php -R 'if (\$argn != \"\") echo(json_decode(urldecode(\$argn))[1].\"\\n\");'
*)
(* ****** ****** *)
//
#define
ATS_DYNLOADFLAG 0
//
(* ****** ****** *)
//
#define
ATS_PACKNAME "MYSOLUTION"
//
(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
//
(* ****** ****** *)

#include "./../assign.dats"

(* ****** ****** *)
//
// Please come up with your own test
//
(* ****** ****** *)
(*
//
// Points: 10
//
The following is a well-known series:

ln 2 = 1 - 1/2 + 1/3 - 1/4 + ...

Please implement a stream consisting of all the partial sums of this
series. Then compute an accurate approximation to ln2 by using Euler's
transform (only if you do the following bonus question).
//
*)
implement
stream_ln2(): stream(double) =
let fun helper(lvl: double, n: double): stream(double)=
$delay( if lvl > 0.0
        then stream_cons(n + (1/lvl),helper((lvl+1)*(~1),n + (1/lvl)))
        else stream_cons(n + (1/lvl),helper((lvl-1)*(~1),n + (1/lvl)))
        )
in
  helper(1.0, 0.0)
end

(* ****** ****** *)
//
// Points: 10
//
// Please implement the following function that enumerates
// all the pairs of natural numbers (i, j) satisfying i <= j:
//
typedef int2 = $tup(int, int)
//
implement
intpair_enumerate ((*void*)): stream(int2) =
let fun helper(n: int2): stream(int2) =
$delay(
  if n.0+1 < 0
  then stream_nil()
  else if n.1+1 < 0
  then stream_cons($tup(n.0+1,n.0+1),helper($tup(n.0+1,n.0+1)))
  else stream_cons($tup(n.0,n.1+1), helper($tup(n.0,n.1+1)))
  )
in
  helper($tup(0,~1))
end
//
(* ****** ****** *)
//
// HX: 10 *bonus* points
// Please implement the Euler transform:
// Given a sequence:
//   x(0), x(1), x(2), ..., x(n), ...
// We can form another sequence:
//   y(0), y(1), y(2), ..., y(n), ....
// such that each y(n) equals the the follow value
//
// x(n+2) - (x(n+2)-x(n+1))^2 / (x(n)+x(n+2)-2*x(n+1))
//
fun mysquare(n: double):double = n*n
implement
EulerTrans(xs: stream(double)): stream(double)=
$delay(
  case !xs of
  |stream_nil() => stream_nil()
  |stream_cons(x0,xs1) => case !xs1 of
                          |stream_nil() => stream_nil()
                          |stream_cons(x1,xs2) => case !xs2 of
                                                  |stream_nil() => stream_nil()
                                                  |stream_cons(x2,_) => stream_cons(x2 - (mysquare(x2-x1)/(x0+x2-(2*x1))),EulerTrans(xs1))
                                                  )
    

//
(* ****** ****** *)
#ifdef
MAIN_NONE
#then
#else
//
implement
main0 () =
{
//
val () =
println!
(
  "Hello from [assign_sol]!"
) (* val *)
//
val
ln2_1M =
stream_nth_exn(stream_ln2(), 1000000)
val () = println! ("ln2_1M = ", ln2_1M)
//
val xys =
  intpair_enumerate()
val xys_10 = stream_take_exn(xys, 10)
val xys_10 = list0_of_list_vt(xys_10)
//
val () = println! ("xys_10 = ", xys_10)
//

val ln2 = stream_ln2()
val ln2 = EulerTrans(ln2)
val ln2 = EulerTrans(ln2)
val ln2 = EulerTrans(ln2)
val ln2 = EulerTrans(ln2)
val ((*void*)) =
  println! ("ln2_4_0 = ", stream_nth_exn(ln2, 0))

//
val ((*void*)) =
  println! ("Good news: Your code has passed initial testing!")
//
} (* end of [main0] *)
//
#endif // #ifdef

(* ****** ****** *)

(* end of [assign_sol.dats] *)
