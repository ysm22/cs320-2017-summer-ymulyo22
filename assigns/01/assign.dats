(* ****** ****** *)
//
// Title:
// Concepts of
// Programming Languages
// Course: CAS CS 320
//
// Semester: Summer I, 2017
//
// Classroom: KCB 102
// Class Time: MTWR 2:00-4:00
//
// Instructor: Hongwei Xi (hwxiATcsDOTbuDOTedu)
//
(* ****** ****** *)
//
// Assign01: 50 points
//
// Out date: Tuesday, May 23rd, 2017
// Due date: Tuesday, May 30th, 2017 // by midnight
//
(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
//
(* ****** ****** *)
//
extern
fun
fact(n: int): int
//
implement
fact(n) =
if n > 0 then n * fact(n-1) else 1
//
(* ****** ****** *)
//
// HX: 5 points
// Please implement a function [try_fact] that
// finds the first [n] such that fact(n) equals 0:
//
extern
fun
try_fact((*void*)): int
//
(* ****** ****** *)
//
// HX: 5 points
// Please implement a function [sumup_3_5_fact] that
// sums up all the natural numbers less than [n0] that
// is a multiple of 3 or 5 but not both.
//
extern
fun
sumup_3_5(n0: int): int
//
(* ****** ****** *)
//
(*
Old English money had 12 pence in a shilling and 20
shillings in a pound.
*)
//
extern
fun
money2pence // 5 points
  (m0: $tup(int(*pound*), int(*shilling*), int(*pence*))): int
//
extern
fun
pence2money // 5 points
  (n0: int): $tup(int(*pound*), int(*shilling*), int(*pence*))
//
(* ****** ****** *)
//
typedef
point =
$tup(double, double, double)
//
typedef
triangle = $tup(point, point, point)
//
(* ****** ****** *)
//
// HX: 10 points
//
extern
fun
test_colinearity
  (p1: point, p2: point, p3: point): bool
//
(* ****** ****** *)
//
// HX: 10 points
//
extern
fun
triangle_median_inner(A: triangle): triangle
//
// HX: 10 points
//
extern
fun
triangle_median_outer(A: triangle): triangle
//
(* ****** ****** *)

(* end of [assign.dats] *)
