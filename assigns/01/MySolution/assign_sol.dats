(* ****** ****** *)
//
// HX:
//
// How to test:
// ./assign_sol
// How to compile:
// myatscc assign_sol.dats
//
(* ****** ****** *)
(*
\#\#myatsccdef=\
curl --data-urlencode mycode@$1 \
http://bucs320-tutoriats.rhcloud.com/\
SERVICE/assets/patsopt_tcats_eval_code_0_.php | \
php -R 'if (\$argn != \"\") echo(json_decode(urldecode(\$argn))[1].\"\\n\");'
*)
(* ****** ****** *)
//
#define
ATS_DYNLOADFLAG 0
//
(* ****** ****** *)
//
#define
ATS_PACKNAME "MYSOLUTION"
//
(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
//
(* ****** ****** *)

#staload "./../assign.dats"

(* ****** ****** *)
//

//
// HX: This is a dummy
//
implement
fact(n) =
  if n > 0 then n * fact(n-1) else 1

implement try_fact() = 
  let fun helper(n: int): int =
    if(fact(n) = 0)
    then n
    else 
    helper(n+1)
  in
    helper(1)
  end
//

//
(* ****** ****** *)
//

//
// HX: This is a dummy
//
implement sumup_3_5(n0) = 
  let fun helper(n: int, total: int): int =
    if(n = 0)
    then total
    else if((n mod 3)=0 || (n mod 5)=0 && not((n mod 3)=0 && (n mod 5)=0))      //n mod 3  = 0 xor n mod 5 = 0
    then helper(n - 1, total + n)
    else
    helper(n - 1, total)
  in
    helper(n0, 0)
  end
//

//
(* ****** ****** *)

// money2pence // 5 points
implement
money2pence(m0: $tup(int(*pound*), int(*shilling*), int(*pence*))): int =
  //val (pound: int, shilling: int, pence: int) = m0
  (m0.0 * 240) + (m0.1 * 12) + m0.2
// pence2money // 5 points
implement
pence2money(n0: int): $tup(int(*pound*), int(*shilling*), int(*pence*)) =
  '(n0/240, (n0 mod 240)/12, ((n0 mod 240) mod 12))

(* ****** ****** *)

// test_colinearity // 10 points
//https://glot.io/snippets/ekjoyrj10z <--- where i get the function to square root
#include "share/atspre_staload.hats"
#include "share/atspre_define.hats"

#define EPS 1E-6

typedef fdouble = double -<cloref1> double

fun newton_raphson (f: fdouble, f': fdouble, x0: double): double = let
    fun loop (f: fdouble, f': fdouble, x0: double): double = let
        val y0 = f x0
    in
        if abs (y0 / x0) < EPS then x0 else
            let val y1 = f' x0 in loop (f, f', x0 - y0 / y1)
        end
    end
in
    loop (f, f', x0)
end

fn my_sqrt (c: double): double = newton_raphson (lam x => x * x - c, lam x => 2.0 * x, 1.0)
//outside source ends here

fun square (n:double):double= n*n 
implement
test_colinearity(p1: point, p2: point, p3: point): bool =
  let fun helper(mag1:double, mag2:double, mag3:double): bool =
    if(mag1 + mag2 > mag3)
    then abs(mag2 - mag3) - mag1 = 0.0
    else
    mag3 = mag1 + mag2
  in
    helper(my_sqrt(square(p1.0-p2.0) + square(p1.1-p2.1) + square(p1.2-p2.2)),my_sqrt(square(p1.0-p3.0) + square(p1.1-p3.1) + square(p1.2-p3.2)),my_sqrt(square(p3.0-p2.0) + square(p3.1-p2.1) + square(p3.2-p2.2)))
  end

(* ****** ****** *)

// triangle_median_inner // 10 points
implement
triangle_median_inner(A: triangle): triangle= 
  let fun helper(x: double, y: double, z: double): point=
    $tup(x/2,y/2,z/2)
  in
    $tup(helper(A.0.0 + A.1.0,A.0.1 + A.1.1, A.0.2 + A.1.2),helper(A.0.0 + A.2.0, A.0.1 + A.2.1, A.0.2 + A.2.2),helper(A.1.0 + A.2.0, A.1.1 + A.2.1, A.1.2 + A.2.2))
  end
// triangle_median_outer // 10 points
implement
triangle_median_outer(A: triangle): triangle= 
  let fun helperT(p1: point, p2: point, p3: point): point=
    let fun helperP(c1: double, c2: double, c3: double): double=
      c1 + c2 - c3
    in
      $tup(helperP(p1.0,p2.0,p3.0),helperP(p1.1,p2.1,p3.1),helperP(p1.2,p2.2,p3.2))
    end
  in
    $tup(helperT(A.0,A.1,A.2),helperT(A.1,A.2,A.0),helperT(A.0,A.2,A.1))
  end

(* ****** ****** *)
//
// HX-2017-01-19:
// Please do not modify the following code
//
(* ****** ****** *)

#ifdef
MAIN_NONE
#then
#else

implement
main0() = () where
{
//
val () =
println!
  ("try_fact() = ", try_fact())
//
val () =
println!
  ("sumup_3_5(1000) = ", sumup_3_5(1000))
//

//
val () =
println!
  ("money check ", money2pence(pence2money(1000)) = 1000)
//
val () =
println!
  (abs(my_sqrt(square(6.0-6.0) + square(1.0-3.0) + square(3.0-3.0)) - my_sqrt(square(6.0-6.0) + square(1.0-6.0) + square(3.0-3.0))) - my_sqrt(square(6.0-6.0) + square(3.0-6.0) + square(3.0-3.0))  , test_colinearity($tup(6.0,1.0,3.0),$tup(6.0,3.0,3.0),$tup(6.0,6.0,3.0)))
//
fun
fprint_point
(out: FILEref, P: point): void =
fprint!(out, "(", P.0, ", ", P.1, ", ", P.2, ")")
                        
fun
fprint_triangle
(out: FILEref, T: triangle): void = let
//                            
overload fprint with fprint_point
//                                        
in
  fprint!(out, "(", T.0, ", ", T.1, ", ", T.2, ")")
end // end of [fprint_triangle] 

val A = $tup(1.23, 2.34, 3.45)
val B = $tup(1.23+3, 2.34+4, 3.45+5)
val C = $tup(1.23+7, 2.34+8, 3.45+9)
//                                               
val ABC = $tup(A, B, C)
val ABC2 = triangle_median_inner(triangle_median_outer(ABC))
//                                                        
val () =
  fprintln!(stdout_ref, "ABC:")
val () =
  fprint_triangle(stdout_ref, ABC)
val () = fprint_newline(stdout_ref)
//                           
val () =
  fprintln!(stdout_ref, "ABC2:") // should be the same as ABC
val () =
  fprint_triangle(stdout_ref, ABC2)
val () = fprint_newline(stdout_ref)

} (* end of [main0] *)

#endif // end of #ifdef(MAIN_NONE)

(* ****** ****** *)

(* end of [assign_sol.dats] *)
