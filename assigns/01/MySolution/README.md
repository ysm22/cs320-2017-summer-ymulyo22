#README#

Assign01 Notes:
    try_fact() :
        A tail recursive function that keeps count of n to see how much n needs to be till
        fact(n) returns 0. Therefore if fact(n) = 0, for that n, it will return n.
    
    sumup_3_5(n0) :
        A function where it adds a number divisible by 3 or 5 but not both. 
        Without an inbuilt xor operation, I had to write it manually where the
        logic goes, (n mod 3)=0 || (n mod 5)=0 && not((n mod 3)=0 && (n mod 5)=0).
        If such statement returns true, it will decrement n and increase the total (number place
        holder) by n, else it just decrements n and keeps total the same.
    
    money2pence :
        Multiple the pounds by 240, the shilling by 12, and adding them all with 
        the remaining pence to result in the total pence. The multiplication is done
        through the knowledge that 12 pences are one shilling and 20 shillings are
        one pound.
    
    pence2money :
        The inverse of money2pence. returning a tuple with 3 ints. Pound is found by
        dividing the pence by 240. Since it is in ints, it is automatically floored.
        The second in, shilling, is found by moding the total pence by 240 then dividing
        it by 12. Lastly, the remaining pence is found by the total pence mod by 240 and mod
        again by 12.
    
    test_colinearity :
        This version currently has issues because of the far decimal number differences 
        from square rooting. As shown as the print test, I have compared sqrt(5.0 - 3.0) - 2.0 = 0.0, but the answer
        is false eventhough it is clearly true. The way I tried to solve this was by comparing magnitude of ab and bc and ac.
        These points are colinear if and only if ab + bc = ac or ac - bc = ac or ac - ab = bc.
        
    triangle_median_inner:
        This is pretty simple as we are to find the triangle that lies in the middle of the triagle
        using the midpoint of each side. This was done by assigning each midpoint of
        the triagle as end points of the new inner triangle.
        
    triangle_median_outer
        This is the harder part of this assignment. The arithmathic done by this is reverse of
        what we did in the median inner. for example, let GHI be the outer triangle as
        ABC is the inner triagle. Let A be parallel to H, B to I and C to G.
        since (G + I)/2 = A, (G+H)/2 = B, (H+I)/2 = C, it can be 
        rewriten as (G + I) = 2A, (G+H) = 2B, (H+I) = 2C.
        From here, through algebra, we can find that G = A + B - C
        H = B + C - A, I = A + C - B.
    
    
    
