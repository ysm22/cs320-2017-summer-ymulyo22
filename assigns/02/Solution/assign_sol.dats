(* ****** ****** *)
//
// HX:
//
// How to test:
// ./assign_sol
// How to compile:
// myatscc assign_sol.dats
//
(* ****** ****** *)
(*
\#\#myatsccdef=\
curl --data-urlencode mycode@$1 \
http://bucs320-tutoriats.rhcloud.com/\
SERVICE/assets/patsopt_tcats_eval_code_0_.php | \
php -R 'if (\$argn != \"\") echo(json_decode(urldecode(\$argn))[1].\"\\n\");'
*)
(* ****** ****** *)
//
#define
ATS_DYNLOADFLAG 0
//
(* ****** ****** *)
//
#define
ATS_PACKNAME "MYSOLUTION"
//
(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
//
(* ****** ****** *)

#include "./../assign.dats"

(* ****** ****** *)
//
implement
mylist0_last
  (xs) =
(
let
//
val-
list0_cons(x0, xs) = xs in loop(x0, xs)
//
end where
{
  fun
  loop(x0: int, xs: list0(int)): int =
    case+ xs of
    | list0_nil() => x0
    | list0_cons(x1, xs) => loop(x1, xs)
} (* end of [where] *)
)
//
(* ****** ****** *)
//
implement
mylist0_length(xs) =
list0_foldleft<int><int>(xs, 0, lam(n, _) => n + 1)
//
(* ****** ****** *)

implement
mylist0_cross
  (xs, ys) = let
//
typedef
int2 = $tup(int,int)
//
macdef
concat = list0_concat<int2>
//
macdef map1 = list0_map<int><int2>
macdef map2 = list0_map<int><list0(int2)>
//
in 
concat
(
map2
( xs
, lam(x) => map1(ys, lam(y) => $tup(x, y))
)
)
end // end of [mylist0_cross]

(* ****** ****** *)

implement
mylist0_choose
  (xs, n) =
(
case+ xs of
| list0_nil() =>
    if n = 0
      then list0_sing(list0_nil) else list0_nil()
    // end of [if]
| list0_cons(x0, xs) =>
    mylist0_mcons(x0, mylist0_choose(xs, n-1)) + mylist0_choose(xs, n)
  // end of [list0_cons]
)

(* ****** ****** *)

#ifdef
MAIN_NONE
#then
#else

implement
main0() = () where
{
//
val
xs0 =
g0ofg1($list{int}(1,2,3,4,5))
val
ys0 =
g0ofg1($list{int}(1,2,3,4,5))
//
val () =
assertloc(mylist0_last(xs0) = 5)
//
#define N 1000000
val () =
assertloc
(
mylist0_length(list0_make_intrange(0, N)) = N
) (* end of [val] *)
//
val xss = mylist0_choose(xs0, 2)
val ((*void*)) =
(
  xss.foreach()(lam(xs) => println!(xs))
)
//
val xys = mylist0_cross(xs0, ys0)
val ((*void*)) =
(
  xys.foreach()(lam(xy) => println!("(", xy.0, ", ", xy.1, ")"))
)
//
} (* end of [main0] *)

#endif // end of #ifdef(MAIN_NONE)

(* ****** ****** *)

(* end of [assign_sol.dats] *)
