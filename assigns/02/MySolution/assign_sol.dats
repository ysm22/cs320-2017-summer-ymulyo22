(* ****** ****** *)
//
// HX:
//
// How to test:
// ./assign_sol
// How to compile:
// myatscc assign_sol.dats
//
(* ****** ****** *)
(*
\#\#myatsccdef=\
curl --data-urlencode mycode@$1 \
http://bucs320-tutoriats.rhcloud.com/\
SERVICE/assets/patsopt_tcats_eval_code_0_.php | \
php -R 'if (\$argn != \"\") echo(json_decode(urldecode(\$argn))[1].\"\\n\");'
*)
(* ****** ****** *)
//
#define
ATS_DYNLOADFLAG 0
//
(* ****** ****** *)
//
#define
ATS_PACKNAME "MYSOLUTION"
//
(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
//
(* ****** ****** *)

#include "./../assign.dats"

(* ****** ****** *)
implement
mylist0_last(xs: list0(int)) = 
  let fun helper(xs: list0(int),lastN: int): int =
  case xs of
  | list0_nil() => lastN
  | list0_cons(x1, xs2) => helper(xs2, x1)
  in
    helper(xs,0)
  end

implement
mylist0_length(xs: list0(int)) =
  let fun helper(xs: list0(int), nLength: int): int = 
  case xs of
  | list0_nil() => nLength
  | list0_cons(x1, xss) => helper(xss, nLength+1)
  in
    helper(xs,0)
  end

implement
mylist0_cross
  (xs: list0(int), ys: list0(int)) = 
  case xs of
  | list0_nil() => list0_nil()
  | list0_cons(x1,xs2) => list0_map(ys, lam x => $tup(x1,x)) + mylist0_cross(xs2,ys)


fun
double_list_perE(xs: list0(int)): list0(list0(int)) =
list0_map(xs, lam x => list0_cons(x, list0_nil()))

implement
mylist0_choose(xs: list0(int), n: int) =
case xs of
| list0_nil() => list0_nil()
| list0_cons(x1,xs2) => if n <= 0
                        then list0_nil()
                        else if n = 1
                        then double_list_perE(xs)
                        else if n = 2
                        then mylist0_mcons(x1,double_list_perE(xs2)) + mylist0_choose(xs2,n)
                        else mylist0_mcons(x1,mylist0_choose(xs2,n-1)) + mylist0_choose(xs2,n)
  
  

#ifdef
MAIN_NONE
#then
#else

implement
main0() = () where
{
//
val
xs0 =
g0ofg1($list{int}(1,2,3,4,5))
val
ys0 =
g0ofg1($list{int}(1,2,3,4,5))
//
val () =
assertloc(mylist0_last(xs0) = 5)
//
#define N 1000000
val () =
assertloc
(
mylist0_length(list0_make_intrange(0, N)) = N
) (* end of [val] *)
//As
val xss = mylist0_choose(xs0, 3)
val ((*void*)) =
(
  xss.foreach()(lam(xs) => println!(xs))
)
//
val xys = mylist0_cross(xs0, ys0)
val ((*void*)) =
(
  xys.foreach()(lam(xy) => println!("(", xy.0, ", ", xy.1, ")"))
)

//
} (* end of [main0] *)

#endif // end of #ifdef(MAIN_NONE)

(* ****** ****** *)

(* end of [assign_sol.dats] *)
