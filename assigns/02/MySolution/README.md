#README#
Assignment2():
    Notes():
            mylist0_last(xs: list0(int)): is a tail recursive function that has a helper inside that takes in a list and a number.
            That number starts as 0 by default and immidiately gets replaced after the first call of the helper, as it 
            is changed into the first element of the list. The number that keeps on being replaced
            as it traverses through the list, storing what's at its head till it is nill, which is when
            it returns the last number.
            
            mylist0_length(xs: list0(int)): is similar to mylist0_last but instead of replacing the storing number,
            it increments it as it start at 0. As it reaches the null list (the tail), it returns the total number/length.
            
            mylist0_cross(xs: list0(int), ys: list0(int)): has a helper function with similar parameters to mylist0_cross but with
            an extra parameter called holderY which holds the original ys. This is so that the list ys is able to be traversed
            as many times as possible. The whole point in this code is that it creates a double tuple with the first
            element of xs and all the elements of ys before the next element of xs comes into play.
            
            mylist0_choose(xs: list0(int), n: int): uses two helper function where one is to create the desired pair list,
            and the other returns the concatenation of the list on another list, which makes a list of lists.