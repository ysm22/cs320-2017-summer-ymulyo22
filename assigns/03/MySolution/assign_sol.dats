(* ****** ****** *)
//
// HX:
//
// How to test:
// ./assign_sol
// How to compile:
// myatscc assign_sol.dats
//
(* ****** ****** *)
(*
\#\#myatsccdef=\
curl --data-urlencode mycode@$1 \
http://bucs320-tutoriats.rhcloud.com/\
SERVICE/assets/patsopt_tcats_eval_code_0_.php | \
php -R 'if (\$argn != \"\") echo(json_decode(urldecode(\$argn))[1].\"\\n\");'
*)
(* ****** ****** *)
//
#define
ATS_DYNLOADFLAG 0
//
(* ****** ****** *)
//
#define
ATS_PACKNAME "MYSOLUTION"
//
(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
//
(* ****** ****** *)

#include "./../assign.dats"

//
// 10 points
//
// HX: please check if [x0] occurs in [xs]
//
implement
{a}
mylist0_contain
(
xs: list0 (a), x0: a, eq: (a, a) -<cloref1> bool
) : bool = (list0_length(list0_filter<a>(xs,lam x => eq(x,x0))) >= 1)
    
(*case xs of
|list0_nil() => false
|list0_cons{a}(x1,xs2) => if eq<a>(x1,x0)
                       then true
                       else mylist0_contain(xs2,x0,eq)*)

//
(* ****** ****** *)
//
// 10 points
//
// HX: [mylist0_find] checks if there is an element
// in [xs] that satisfies a given predicate [pred]. If
// such an element is found, it returns the index of the
// element. Otherwise, ~1 (negative 1) is returned.
//
// Note that [mylist0_find] searches from left to right.
//
implement
{a}
mylist0_find
  (xs: list0(a), test: (a) -<cloref1> bool): int =
let fun helper(xs:list0(a), i: int): int=
case xs of
|list0_nil() => ~1
|list0_cons(x1,xs2) => if test(x1)
                       then i
                       else helper(xs2,i+1)
in
    helper(xs,0)
end

                        
//
(* ****** ****** *)
//
// 10 points
//
// HX: [mylist0_rfind] checks if there is an element
// in [xs] that satisfies a given predicate [pred]. If
// such an element is found, it returns the index of the
// element. Otherwise, ~1 (negative 1) is returned.
//
// Note that [mylist0_rfind] searches from right to left.
//
implement
{a}
mylist0_rfind
  (xs: list0(a), test: (a) -<cloref1> bool): int =
let fun helper(xs:list0(a), i: int): int=
case xs of
|list0_nil() => ~1
|list0_cons(x1,xs2) => if test(x1)
                       then i
                       else helper(xs2,i-1)
in
    helper(list0_reverse(xs),list0_length(xs)-1)
end
//
(* ****** ****** *)
fn isEquals(x:int,y:int):bool= (x=y)
(* ****** ****** *)
implement main0() = {
val xs0 = g0ofg1($list{int}(1,2,3,4,5))
val xs2 = g0ofg1($list{int}(1,1,5))
val xys = mylist0_contain(xs0,1,lam (x,y) => isEquals(x,y))
val xyy = mylist0_find(xs2,lam x => isEquals(x, 1))
val xyz = mylist0_rfind(xs2,lam x => isEquals(x, 1))
val () = println! (xys,xyy,xyz)
}
(* ****** ****** *)

(* end of [assign_sol.dats] *)
