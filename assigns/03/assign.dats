(* ****** ****** *)
//
// Title:
// Concepts of
// Programming Languages
// Course: CAS CS 320
//
// Semester: Summer I, 2017
//
// Classroom: KCB 102
// Class Time: MTWR 2:00-4:00
//
// Instructor:
// Hongwei Xi (hwxiATcsDOTbuDOTedu)
//
(* ****** ****** *)
//
// Assign03: 30 points
//
// Out date: Tue, June 6th, 2017
// Due date: Fri, June 9th, 2017 // by midnight
//
(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
#include
"share/HATS\
/atspre_staload_libats_ML.hats"
//
(* ****** ****** *)

//
// 10 points
//
// HX: please check if [x0] occurs in [xs]
//
extern
fun{a:t@ype}
mylist0_contain
(
xs: list0 (a), x0: a, eq: (a, a) -<cloref1> bool
) : bool
//
(* ****** ****** *)
//
// 10 points
//
// HX: [mylist0_find] checks if there is an element
// in [xs] that satisfies a given predicate [pred]. If
// such an element is found, it returns the index of the
// element. Otherwise, ~1 (negative 1) is returned.
//
// Note that [mylist0_find] searches from left to right.
//
extern
fun{a:t@ype}
mylist0_find
  (xs: list0(a), test: (a) -<cloref1> bool): int
//
(* ****** ****** *)
//
// 10 points
//
// HX: [mylist0_rfind] checks if there is an element
// in [xs] that satisfies a given predicate [pred]. If
// such an element is found, it returns the index of the
// element. Otherwise, ~1 (negative 1) is returned.
//
// Note that [mylist0_rfind] searches from right to left.
//
extern
fun{a:t@ype}
mylist0_rfind
  (xs: list0(a), test: (a) -<cloref1> bool): int
//
(* ****** ****** *)

(* end of [assign.dats] *)
