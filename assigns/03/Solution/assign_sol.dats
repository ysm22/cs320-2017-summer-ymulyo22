(* ****** ****** *)
//
// HX:
//
// How to test:
// ./assign_sol
// How to compile:
// myatscc assign_sol.dats
//
(* ****** ****** *)
(*
\#\#myatsccdef=\
curl --data-urlencode mycode@$1 \
http://bucs320-tutoriats.rhcloud.com/\
SERVICE/assets/patsopt_tcats_eval_code_0_.php | \
php -R 'if (\$argn != \"\") echo(json_decode(urldecode(\$argn))[1].\"\\n\");'
*)
(* ****** ****** *)
//
#define
ATS_DYNLOADFLAG 0
//
(* ****** ****** *)
//
#define
ATS_PACKNAME "MYSOLUTION"
//
(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
//
(* ****** ****** *)

#include "./../assign.dats"

(* ****** ****** *)

(*
implement
{a}
mylist0_contain
  (xs, x0, eq) =
(
case+ xs of
| list0_nil() => false
| list0_cons(x, xs) =>
  (eq(x0, x) orelse
   mylist0_contain<a>(xs, x0, eq))
)
*)

(* ****** ****** *)

exception TRUE of ()
exception FOUND of int

(* ****** ****** *)

implement
{a}
mylist0_contain
  (xs, x0, eq) =
(
try
list0_foldleft<bool><a>
  (xs, false, lam(res, x) => if eq(x0, x) then $raise TRUE() else res)
with ~TRUE() => true(*found*)
)

(* ****** ****** *)

(*
implement
{a}
mylist0_find
  (xs, test) =
  loop(xs, 0) where
{
fun
loop(xs: list0(a), i: int): int =
(
case+ xs of
| list0_nil() => ~1
| list0_cons(x, xs) =>
  if test(x) then i else loop(xs, i+1)
)
} (* end of [mylist0_find] *)
*)

(* ****** ****** *)

implement
{a}
mylist0_find
  (xs, test) =
(
try
let val _ =
list0_foldleft<int><a>
  (xs, 0, lam(i, x) => if test(x) then $raise FOUND(i) else i+1)
in
  ~1
end with ~FOUND(i) => i(*found*)
)

(* ****** ****** *)

(*
implement
{a}
mylist0_rfind
  (xs, test) = let
//
fun
auxlst
(xs: list0(a), i: int): int =
(
case+ xs of
| list0_nil() => ~1
| list0_cons(x, xs) =>
  if test(x) then i-1 else auxlst(xs, i-1)
)
//
in
  auxlst(xs, length(xs))
end (* end of [mylist0_rfind] *)
*)

(* ****** ****** *)

implement
{a}
mylist0_rfind
  (xs, test) =
(
try
//
list0_foldright<a><int>
( xs
, lam(x, i) => if test(x) then $raise FOUND(i) else i-1
, length(xs)-1
)
//
with ~FOUND(i) => i(*found*)
)

(* ****** ****** *)
//
// Please come up with your own test
//
(* ****** ****** *)

implement
main0() =
{
//
val xs =
g0ofg1($list{int}(0, 2, 4, 6, 8))
//
val () =
assertloc
(
mylist0_contain<int>(xs, 6, lam(x, y) => x = y)
) (* assertloc *)
val () =
assertloc
(
~mylist0_contain<int>(xs, 9, lam(x, y) => x = y)
) (* assertloc *)
//
val () =
assertloc
(
~1(*index*) = mylist0_find<int>(xs, lam(x) => x = 10)
) (* assertloc *)
//
val () =
assertloc
(
2(*index*) = mylist0_find<int>(xs, lam(x) => x = 4)
) (* assertloc *)
//
val () =
assertloc
(
2(*index*) = mylist0_rfind<int>(xs, lam(x) => x = 4)
) (* assertloc *)
//
} (* end of [main0] *)

(* ****** ****** *)

(* end of [assign_sol.dats] *)
