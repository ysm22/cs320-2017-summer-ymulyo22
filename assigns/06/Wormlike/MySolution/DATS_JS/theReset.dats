(* ****** ****** *)
(*
** Scene setup
*)
(* ****** ****** *)
//
#include "./theParams.hats"
//
#include
"{$LIBATSCC2JS}/staloadall.hats"
//
(* ****** ****** *)
//
#staload "./main.sats"
//
(* ****** ****** *)

(*
implement
theBoard_initialize
  ((*void*)) = let
//
val
theBoard = theBoard_get()
//
val N = min(XLEN, YLEN)
fun
add_blocks() =
(N).foreach()
(
lam(i) =>
if i mod 2 = 0 then
  (
   theBoard[0, i] := BLOCK;
   theBoard[XLEN-1, i] := BLOCK;
  )
) (* add_blocks *)
//
fun
xrand(): int =
  double2int(XLEN*JSmath_random())
fun
yrand(): int =
  double2int(YLEN*JSmath_random())
//
fun
add_prize
(
// argless
) : void = let
  val x = xrand()
  and y = yrand()
//
  val knd = theBoard[x, y]
//
in
  if knd = 0
    then theBoard[x, y] := PRIZE else add_prize()
  // end of [if]
end // end of [add_prize]
//
fun
add_prizes() = (50).repeat()(lam() => add_prize())
//
in
  add_blocks(); add_prizes()
end // end of [theBoard_initialize]
*)

(* ****** ****** *)

implement
theBoard_initialize
  ((*void*)) = let
//
val
theBoard = theBoard_get()
//
val N = min(XLEN, YLEN)
val xwidth = 6
val ylength = 8
val xoff = XLEN/2 + xwidth
val yoff = YLEN/2 + ylength
val spaces = 2

fun make_line_n_8(n: int) =
let fun itt(l: int) =
  if (l > 0) then (theBoard[xoff-l,n] := BLOCK; itt(l-1);)
in
  itt(xwidth)
end

fun make_line_n_y(n: int) =
let fun itt(l: int) =
  if (l > 0) then (theBoard[xoff-xwidth-spaces-l,n] := BLOCK; itt(l-1);)
in
  itt(xwidth)
end




fun
add_blocks1() =
(N).foreach()
(
lam(i) =>
if i >= yoff - ylength && i < yoff then
  (
   if i = yoff - ylength || i = yoff - 1 || i = yoff - ylength/2
   then (theBoard[xoff-xwidth-spaces,i] := BLOCK;make_line_n_8(i))
   else (theBoard[xoff-xwidth-spaces,i] := BLOCK;theBoard[xoff-xwidth,i] := BLOCK;theBoard[xoff-1,i] := BLOCK;)
  )
) (* add_blocks *)

fun
add_blocks2() =
(N).foreach()
(
lam(i) =>
if i >= yoff - ylength && i < yoff then
  (
   if i < yoff - ylength + xwidth/2 && i > yoff - ylength
         then (theBoard[xoff-xwidth,i] := BLOCK; theBoard[xoff-1,i] := BLOCK; theBoard[xoff-xwidth-i+yoff-spaces-1,i] := BLOCK; theBoard[xoff+i-yoff+spaces,i] := BLOCK; theBoard[xoff-xwidth-spaces,i] := BLOCK;theBoard[xoff-(2*xwidth)-spaces,i] := BLOCK;)
         else if i = yoff - ylength/2  || i = yoff - 1
         then (theBoard[xoff-xwidth-spaces,i] := BLOCK;theBoard[xoff-(2*xwidth)-spaces,i] := BLOCK; make_line_n_y(i);theBoard[xoff-xwidth,i] := BLOCK; theBoard[xoff-1,i] := BLOCK;make_line_n_y(i))
         else if i < yoff - ylength/2
         then (theBoard[xoff-xwidth,i] := BLOCK; theBoard[xoff-1,i] := BLOCK;theBoard[xoff-xwidth-spaces,i] := BLOCK;theBoard[xoff-(2*xwidth)-spaces,i] := BLOCK)
         else (theBoard[xoff-xwidth,i] := BLOCK; theBoard[xoff-1,i] := BLOCK;theBoard[xoff-xwidth-spaces,i] := BLOCK)
         
))
//
fun
xrand(): int =
  double2int(XLEN*JSmath_random())
fun
yrand(): int =
  double2int(YLEN*JSmath_random())
//
fun
add_prize
(
// argless
) : void = let
  val x = xrand()
  and y = yrand()
//
  val knd = theBoard[x, y]
//
in
  if knd = 0
    then theBoard[x, y] := PRIZE else add_prize()
  // end of [if]
end // end of [add_prize]
//
fun
add_prizes() = (50).repeat()(lam() => add_prize())
//
in
  if double2int(2.0*JSmath_random()) mod 2 < 1
  then (add_blocks1(); add_prizes())
  else (add_blocks2(); add_prizes())
end // end of

(*implement
theBoard_initialize
  ((*void*)) =
(
  alert("Please display a scene of your initials!")
)*)

(* ****** ****** *)

(* end of [theReset.dats] *)
