######
# //
# // Title:
# // Concepts of
# // Programming Languages
# // Course: CAS CS 320
# //
# // Semester: Summer I, 2017
# //
# // Classroom: KCB 102
# // Class Time: MTWR 2:00-4:00
# //
# // Instructor: Hongwei Xi (hwxiATcsDOTbuDOTedu)
# //
######
#
# Assignment 6
#
# Due: the last day of class (June, 29, 2017)
#
######
#
# Wormlike: 40 points in total
#
# This Assignment is due Thursday, the 20th of April
#
# ######
#
# Scene 1:
#
# 10 points for a scene that consists of the last digit
# X of your BU ID number and 9-X (drawn with gray dots) and
# 50 prizes (drawn with red dots randomly positioned)
#
# Scene 2:
#
# 10 points for a scene that consists of your initials
# (two letters) in capital case (drawn with gray dots)
# and 50 prizes (drawn with red dots randomly positioned)
#
# Search:
#
# 40 points for implementing an automatic search for prizes. The
# algorithm for the search should be based on breadth-first graph
# search.
#
######

###### end of [README.txt] ######
