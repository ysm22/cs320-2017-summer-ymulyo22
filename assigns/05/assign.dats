(* ****** ****** *)
//
// Title:
// Concepts of
// Programming Languages
// Course: CAS CS 320
//
// Semester: Summer I, 2017
//
// Classroom: KCB 102
// Class Time: MTWR 2:00-4:00
//
// Instructor:
// Hongwei Xi (hwxiATcsDOTbuDOTedu)
//
(* ****** ****** *)
//
// Assign05: 30 points
//
// Out date: Mon, June 19th, 2017
// Due date: Fri, June 23th, 2017 // by midnight
//
(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
#include
"share/HATS\
/atspre_staload_libats_ML.hats"
//
(* ****** ****** *)

(*

The coin change problem can be found here:
http://ats-lang.sourceforge.net/DOCUMENT/INT2PROGINATS/HTML/HTMLTOC/x597.html

*)

(* ****** ****** *)
//
// Given a list of coins and a
// total, [coin_change] checks
// if the total can be broken into the
// sum of a sublist of the given coins.
// 
//
extern
fun
coin_change
(
coins: list0(int), total: int
) : Option(list0(int))
//
(* ****** ****** *)

(*
** The following implementation of
** [coin_change] uses option to do backtracking.
*)

(*
implement
coin_change
  (coins, total) =
(
ifcase
| total < 0 => None()
| total = 0 => Some(list0_nil())
| _(* total > 0 *) =>
  (
    case+ coins of
    | list0_nil() => None()
    | list0_cons(coin, coins1) =>
      (
        if total < coin
          then
          (
            coin_change(coins1, total)
          ) (* end of [then] *)
          else (
            case+
            coin_change(coins, total-coin)
            of
            | None() => coin_change(coins1, total)
            | Some(coins) => Some(list0_cons(coin, coins))
          ) (* end of [else] *)
      )
  )
) (* end of [coin_change] *)
*)

(* ****** ****** *)
//
(*
//
// HX: 10 points
//
** Please complete the following code
** that uses exception to support backtracking
//
*)
//
(*
implement
coin_change
(coins, total) = let
//
exception FAILEXN
//
fun
aux
(
coins: list0(int), total: int
) : list0(int) = ...
//
in
//
try Some(aux(coins, total)) with ~FAILEXN() => None()
//
end // end of [coin_change]
*)
//
(* ****** ****** *)

(*
** The coin change problem can also given
** a stream-based implementation where the
** returned stream contains *all* of the possible
** solutions
*)

(* ****** ****** *)
//
extern
fun
coin_change2
(coins: list0(int), total: int): stream(list0(int))
//
(* ****** ****** *)

(*
//
// HX: 10 points
// Please give an implementation of coin_change2
//
*)

(* ****** ****** *)

typedef
config = list0(int)

(*
//
// HX: 10 points
//
Please give an implementation QueenPuzzle_solve
that returns the first solution extending a given
config to the QueenPuzzle problem (see lecture--6-14).
Your solution is required to use exception to support
backtracking.

*)

extern
fun
QueenPuzzle_solve
(config: config): Option(config)

(* ****** ****** *)

(* end of [assign.dats] *)
