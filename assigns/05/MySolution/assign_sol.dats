(* ****** ****** *)
//
// HX:
//
// How to test:
// ./assign_sol
// How to compile:
// myatscc assign_sol.dats
//
(* ****** ****** *)
(*
\#\#myatsccdef=\
curl --data-urlencode mycode@$1 \
http://bucs320-tutoriats.rhcloud.com/\
SERVICE/assets/patsopt_tcats_eval_code_0_.php | \
php -R 'if (\$argn != \"\") echo(json_decode(urldecode(\$argn))[1].\"\\n\");'
*)
(* ****** ****** *)
//
#define
ATS_DYNLOADFLAG 0
//
(* ****** ****** *)
//
#define
ATS_PACKNAME "MYSOLUTION"
//
(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
//
(* ****** ****** *)

#include "./../assign.dats"

(* ****** ****** *)
//
// Please come up with your own test
//
(* ****** ****** *)
implement
coin_change
  (coins, total) =
let exception FAILEXN
fun aux(coins: list0(int), total: int, anySmaller: bool): list0(int) =
(
ifcase
| total < 0 => $raise(FAILEXN)
| total = 0 => list0_nil()
| _(* total > 0 *) =>
  (
    case+ coins of
    | list0_nil() => if anySmaller then list0_cons(0,list0_nil()) else $raise(FAILEXN)
    | list0_cons(coin, coins1) =>
      (
        if total < coin
          then
          (
            aux(coins1, total, anySmaller)
          ) (* end of [then] *)
          else (
            case+
            aux(coins, total-coin, true)
            of
            | list0_nil() => list0_cons(coin,list0_nil())
            | list0_cons(x,xs) => if x = (0)
                                  then aux(coins1, total, false)
                                  else list0_cons(coin, list0_cons(x,xs))
          ) (* end of [else] *)
      )
  )
)
in
  try Some(aux(coins, total, false)) with ~FAILEXN() => None()
end

fun after_i(xs: list0(int), i: int): list0(int) =
case xs of
|list0_nil() => list0_nil
|list0_cons(x,xs1) => if x = i then xs1 else after_i(xs1,i)

implement
coin_change2
(coins: list0(int), total: int): stream(list0(int)) =
let fun helper(new_coins: list0(int), new_total: int, last_sub: list0(int), last_int: int): stream(list0(int)) =
  case coin_change(new_coins, new_total) of
  |None() => (case last_sub of
             |list0_nil() => coin_change2(after_i(coins,last_int), total)
             |list0_cons(x,xs) => helper(after_i(coins, x), new_total + x, xs, x))
  |Some(x) => $delay(stream_cons(x,helper(after_i(new_coins,list0_last_exn(x)),new_total, last_sub, last_int)))
in
$delay(
  case coin_change(coins, total) of
  |None() => stream_nil()
  |Some(x) => stream_cons(x,helper(after_i(coins, list0_last_exn(x)), list0_last_exn(x), list0_tail_exn(list0_reverse(x)), list0_last_exn(x)))
)
end

(*fun possible_spot(original: config, c: config, p: config, lvl: int): Option(int) =
case c of
|list0_nil() => Some(p.head())
|list0_cons(confign, config1) =>  (case p of
                                   |list0_nil() => None()
                                   |list0_cons(piecesn,pieces1) => if (confign-lvl = piecesn) || (confign+lvl = piecesn)
                                                                  then possible_spot(original, original, pieces1, list0_length(original))
                                                                  else possible_spot(original, config1, p, lvl+1)
                                   )*)

fun possible_spot(c: config, p: int, lvl: int): Option(int) =
case c of
|list0_nil() => Some(p)
|list0_cons(confign, config1) =>  if (confign-lvl = p) || (confign+lvl = p) || (confign = p)
                                  then None()
                                  else possible_spot(config1, p, lvl+1) 



(*implement
QueenPuzzle_solve
(config: config): Option(config) =
let exception MISS 
fun aux(config: config, result: config): config =
  case result of
  |list0_nil() => (case config of
                   |list0_nil() => $raise(MISS)
                   |list0_cons(confign, config1) => aux(config1,list0_cons(confign,result)))
  |list0_cons(resultn, result1) => (case possible_spot(result, result, config, 1) of
                                    |None() => if list0_length(config) = 0 then result else $raise(MISS)
                                    |Some(x) => aux(list0_filter(config,lam n => not(n = x)),list0_cons(x,result)))
in
  if (list0_length(config) > 8) then None() else try Some(aux(config, list0_nil())) with ~MISS() => None()
end
*)
implement
QueenPuzzle_solve
(config: config): Option(config) =
let exception MISS 
fun aux(config: config, result: config, i: int, holder: int): config =
  case result of
 |list0_nil() => (case aux(list0_remove_at_exn(config,i), list0_cons(holder,list0_nil()), list0_length(config)-2, list0_last_exn(list0_remove_at_exn(config,i))) of
                 |list0_nil() => if i-1 < 0 then $raise(MISS) else aux(config, result, i-1, config[i-1])
                 |list0_cons(confign, config1) => list0_cons(confign, config1))
 |list0_cons(resultn, result1) => (case possible_spot(result, holder, 1) of
                                  |None() =>  if i-1 < 0 
                                              then list0_nil()
                                              else aux(config, result, i-1, config[i-1])
                                  |Some(x) => if list0_length(config) = 1
                                              then list0_cons(holder,result)
                                              else (case aux(list0_remove_at_exn(config,i), list0_cons(holder,result), list0_length(config)-2, list0_last_exn(list0_remove_at_exn(config,i))) of
                                                    |list0_nil() => if i < 1 then list0_nil() else aux(config, result, i-1, config[i-1])
                                                    |list0_cons(confign, config1) => list0_cons(confign, config1)))
in
  if (list0_length(config) > 8 || list0_length(config) < 1)
  then None() 
  else try Some(aux(list0_map(config, lam x => x % 8), list0_nil(),list0_length(config)-1,list0_last_exn(config))) with ~MISS() => None()
end
#ifdef
MAIN_NONE
#then
#else
//
implement
main0 () =
{
//
val () =
println!
(
  "Hello from [assign_sol]!"
) (* val *)
//
val xs0 = g0ofg1($list{int}(6))
val () = case coin_change(xs0, 1) of
          |None() => println!("empty")
          |Some(x) => list0_foreach<int> (x, lam (x) => println! (x))

val xs1 = g0ofg1($list{int}(6,5,3))
val () = case coin_change(xs1, 8) of
          |None() => println!("empty")
          |Some(x) => list0_foreach<int> (x, lam (x) => println! (x))
val xs2 = g0ofg1($list{int}(1,3))
val stream_ans = coin_change2(xs2,3)
val sI = stream_nth_exn(stream_ans, 0)
val () = println!(sI)
val sI = stream_nth_exn(stream_ans, 1)
val () = println!(sI)
val xs3 = g0ofg1($list{int}(0,1,2,3,4,5,6,7))
val () = case QueenPuzzle_solve(xs3) of
         |None() => println!("Fail")
         |Some(x) => list0_foreach<int> (x, lam (x) => println! (x))
val ((*void*)) =
  println! ("Good news: Your code has passed initial testing!")
//
} (* end of [main0] *)
//
#endif // #ifdef

(* ****** ****** *)

(* end of [assign_sol.dats] *)
