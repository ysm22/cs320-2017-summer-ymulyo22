(* ****** ****** *)
//
// HX:
// How to compile:
// myatscc assign_sol.dats
//
// How to test it:
// ./assign00_sol
//
(* ****** ****** *)
//
#define
ATS_DYNLOADFLAG 0
//
(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
//
(* ****** ****** *)

#include "./../assign.dats"

(* ****** ****** *)

implement
isTriangle(x, y, z) =
(
ifcase
| x+y <= z => false
| y+z <= x => false
| z+x <= y => false
| _(*else*) => true
)

(* ****** ****** *)

implement
fib_trec(n) =
(
fix
f(
i: int,
f0: int, f1: int
): int =<cloref1>
if i < n then f(i+1, f1, f0+f1) else f0
)(0, 0, 1)

(* ****** ****** *)

(* end of [assign_sol.dats] *)
