#README#

Assign00 Notes:

isTriangle:
    A one-line code that does logical operations that checks and returns true if two sides are added together that it'll be bigger/greater
    then the remaining side, else false.

fib_trec:
    a tail recursion function that returns the nth index of the fibonacci sequence. A helper function is made within the fib_trec function to make
    tail recursion happen. The helper function has 4 parameters, i, x, firstn and secondn. i is the incrementer as the helper function increments up to x.
    firstn and secondn is the 2 numbers previous of the next computed number.
    The helper function has 3 cases with the first being if the number (n) given is or bellow 1, then it would just return n.
    The second one is that if i and x is equal, then it would return firstn + secondn.
    The last (else) does the recursive call where i gets incremented, first = first+second and second = first.