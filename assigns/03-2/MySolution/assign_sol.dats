(* ****** ****** *)
//
// HX:
//
// How to test:
// ./assign_sol
// How to compile:
// myatscc assign_sol.dats
//
(* ****** ****** *)
(*
\#\#myatsccdef=\
curl --data-urlencode mycode@$1 \
http://bucs320-tutoriats.rhcloud.com/\
SERVICE/assets/patsopt_tcats_eval_code_0_.php | \
php -R 'if (\$argn != \"\") echo(json_decode(urldecode(\$argn))[1].\"\\n\");'
*)
(* ****** ****** *)
//
#define
ATS_DYNLOADFLAG 0
//
(* ****** ****** *)
//
#define
ATS_PACKNAME "MYSOLUTION"
//
(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
//
(* ****** ****** *)

#include "./../assign.dats"

(* ****** ****** *)
//
// Please come up with your own test
//
(* ****** ****** *)
(* ****** ****** *)
//
// HX: 10 points
//    
implement
intrep_add
  (x: intrep, y: intrep): intrep = 
let fun helper(longer: intrep, shorter: intrep, remainder: int): intrep=
    case shorter of
    |list0_nil() => (case longer of
                    |list0_nil() => list0_cons(remainder,list0_nil())
                    |list0_cons(l1,ls2) => if remainder > 0
                                           then list0_cons((l1 + remainder) mod 10, helper(ls2, shorter, (l1 + remainder) / 10))
                                           else longer)
    
    |list0_cons(x1,xs2) => list0_cons((x1 + longer.head() + remainder) mod 10, helper(longer.tail(), xs2, (x1 + longer.head() + remainder)/10))
in
    if list0_length(x) > list0_length(y)
    then helper(x,y,0)
    else helper(y,x,0)
end
//
(* ****** ****** *)
//
// HX: 10 points
//    
(*
//
// Please use [intrep_add] to solve
// the following problem:
//
// https://projecteuler.net/problem=16
//
*)
fun intrep_add_itself(x: intrep): intrep =
    intrep_add(x,x)
    

(*fun repeat(f: intrep -> intrep, n: int): intrep -<cloref1> intrep =
   if n > 0
   then lam x => f(repeat(f, n-1)(x))
   else lam x => x*)


fun repeat(f: intrep -> intrep, n: int): intrep -<cloref1> intrep = //Modified code from lab 3
   let
       fun out(i: int, res: intrep): intrep =
           if i = 0
           then res
           else out(i - 1, f(res))
   in
       lam x => out(n, x)
   end
fun add_int(x: int, y: int): int = x + y
implement
pow2digitsum(n: int): int = 
    if n <= 0
    then 1
    else if n = 1
    then 2
    else list0_foldleft<int><int>(repeat(intrep_add_itself,n-1)(list0_cons(2,list0_nil())),0,lam(x,y) => add_int(x,y))

//
(* ****** ****** *)
//
// HX: 10 points
//

fun int2intrep(n: int): intrep = 
    if n = 0 
    then list0_nil()
    else list0_cons(n mod 10,int2intrep(n/10))

fun intrep2int(x: intrep): int =
let fun helper(x: intrep, lvl: int): int=
    case x of
    |list0_nil() => 0
    |list0_cons(x1,xs2) => x1 * lvl + helper(xs2,lvl * 10)
in
    helper(x,1)
end

fun mult_int_intrep(n: int, x: intrep): intrep =
let fun helper(remainder: int, x: intrep): intrep =
case x of
|list0_nil() => if remainder > 0 then list0_cons(remainder mod 10, helper(remainder/10, x)) else list0_nil()
|list0_cons(x1,xs2) => list0_cons((x1 * n + remainder) mod 10,helper((x1 * n + remainder) / 10, xs2))
in
    helper(0,x)
end

implement
intrep_mul
  (x: intrep, y: intrep): intrep = 
let fun helper(x: intrep, y: intrep, level: list0(int), result: intrep): intrep =
case x of
|list0_nil() => result
|list0_cons(x1,xs2) => helper(xs2, y, level + list0_cons(0,list0_nil()), intrep_add(result, level + mult_int_intrep(x1,y)))
in
    helper(x,y,list0_nil(),list0_cons(0,list0_nil()))
end

implement main0() = {
val xs0 = g0ofg1($list{int}(1,2,3,4,5))
val xs1 = g0ofg1($list{int}(6,7,8,9))
val xs2 = g0ofg1($list{int}(6,1))
val xs3 = g0ofg1($list{int}(2))
val xs4 = g0ofg1($list{int}(1,2,3,4,0,0,0,0,5))
val xs5 = g0ofg1($list{int}(0))
val () = println!(intrep_add(xs5,xs3))
val () = println!(intrep_add(xs0,xs1))
val () = println!(intrep_add(xs0,xs4))
val () = println!(pow2digitsum(0))
val () = println!(pow2digitsum(~1))
val () = println!(pow2digitsum(1))
val () = println!(pow2digitsum(4))
val () = println!(pow2digitsum(1000))
val () = println!(intrep_mul(xs2,xs3))
val () = println!(intrep_mul(xs0,xs1))
val () = println!(intrep_mul(xs3,xs5))
}
//
(* ****** ****** *)
(* end of [assign_sol.dats] *)
