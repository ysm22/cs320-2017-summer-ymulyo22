(* ****** ****** *)
//
// HX:
//
// How to test:
// ./assign_sol
// How to compile:
// myatscc assign_sol.dats
//
(* ****** ****** *)
(*
\#\#myatsccdef=\
curl --data-urlencode mycode@$1 \
http://bucs320-tutoriats.rhcloud.com/\
SERVICE/assets/patsopt_tcats_eval_code_0_.php | \
php -R 'if (\$argn != \"\") echo(json_decode(urldecode(\$argn))[1].\"\\n\");'
*)
(* ****** ****** *)
//
#define
ATS_DYNLOADFLAG 0
//
(* ****** ****** *)
//
#define
ATS_PACKNAME "MYSOLUTION"
//
(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
//
(* ****** ****** *)

#include "./../assign.dats"

(* ****** ****** *)

#define BASE 10

(* ****** ****** *)

fun
intrep_succ
(xs: intrep): intrep =
(
case+ xs of
| list0_nil() =>
  list0_sing(1)
| list0_cons(x0, xs1) => let
    val x1 = x0 + 1
  in
    if x1 < BASE
      then list0_cons(x1, xs1)
      else list0_cons(x1-BASE, intrep_succ(xs1))
    // end of [if]
  end
)

(* ****** ****** *)

implement
intrep_add
  (xs, ys) =
(
case+ xs of
| list0_nil() => ys
| list0_cons(x0, xs1) =>
  (
    case+ ys of
    | list0_nil() => xs
    | list0_cons(y0, ys1) => let
        val d0 = x0 + y0
      in
        if d0 < BASE
          then list0_cons(d0, intrep_add(xs1, ys1))
          else list0_cons(d0-BASE, intrep_succ(intrep_add(xs1, ys1)))
        // end of [if]
      end // end of [list0_cons]
  )
) (* end of [intrep_add] *)

(* ****** ****** *)

fun
add_int_intrep
(x0: int, ys: intrep): intrep =
(
case+ ys of
| list0_nil() =>
  list0_sing(x0)
| list0_cons(y, ys) => let
    val d0 = x0 + y
  in
    list0_cons
    ( d0%BASE
    , add_int_intrep_if(d0/BASE, ys)
    )
  end // end of [list0_cons]
)
and
add_int_intrep_if
(x0: int, ys: intrep): intrep =
if x0 > 0 then add_int_intrep(x0, ys) else ys

fun
mul_int_intrep
(x0: int, ys: intrep): intrep =
(
case+ ys of
| list0_nil() =>
  list0_nil()
| list0_cons(y, ys) => let
    val d0 = x0 * y
  in
    list0_cons
    ( d0%BASE
    , add_int_intrep_if(d0/BASE, mul_int_intrep(x0, ys))
    )
  end // end of [list0_cons]
)
and
mul_int_intrep_if
(x0: int, ys: intrep): intrep =
if x0 > 0 then mul_int_intrep(x0, ys) else list0_nil()

(* ****** ****** *)
//
fun
shift(xs: intrep): intrep =
  if isneqz(xs) then list0_cons(0, xs) else list0_nil()
//
(* ****** ****** *)

implement
intrep_mul
  (xs, ys) =
(
case+ xs of
| list0_nil() =>
  list0_nil()
| list0_cons(x, xs) =>
  (
    case+ ys of
    | list0_nil() =>
      list0_nil()
    | list0_cons _ =>
      (
        intrep_add(mul_int_intrep(x, ys), shift(intrep_mul(xs, ys)))
      )
  )
) (* end of [intrep_mul] *)

(* ****** ****** *)
//
// Please come up with your own test
//
(* ****** ****** *)

implement
main0() =
{
//
val i0 = 1234
val j0 = 56789
//
val xs =
list0_of_list_vt{int}
(list_vt_reverse(listize_g0int_rep(i0, BASE)))
val ys =
list0_of_list_vt{int}
(list_vt_reverse(listize_g0int_rep(j0, BASE)))
//
val () = println!("i0 = ", i0)
val () = println!("j0 = ", j0)
//
val () = println!("xs = ", xs)
val () = println!("ys = ", ys)
//
val () = println!("i0 + j0 = ", i0 + j0)
val () = println!("i0 * j0 = ", i0 * j0)
//
val () =
println!("xs + ys = ", list0_foldright<int><int>(intrep_add(xs, ys), lam(d, res) => d+BASE*res, 0))
val () =
println!("xs * ys = ", list0_foldright<int><int>(intrep_mul(xs, ys), lam(d, res) => d+BASE*res, 0))
//
} (* end of [main] *)

(* ****** ****** *)

(* end of [assign_sol.dats] *)
